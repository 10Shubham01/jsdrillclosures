const fs = require("fs");
var data = fs.readFileSync("cache.json");
var cache = JSON.parse(data);
function cacheFunction(cb) {
  if (cb) {
    return (function toInvokeCb() {
      let testArg = 7;
      if (cache.includes(testArg)) {
        return `cache value ${testArg}`;
      } else {
        cache.push(testArg);

        var newdata = JSON.stringify(cache);

        fs.writeFileSync("cache.json", newdata);

        return cb(testArg);
      }
    })();
  }
}
module.exports = cacheFunction;
