function limitFunctioncallCount(cb, n) {
  if (cb && n) {
    const limitFunction = function () {
      for (let i = 0; i < n; i++) {
        cb(i);
      }
    };
    return limitFunction();
  }
}
module.exports = limitFunctioncallCount;
