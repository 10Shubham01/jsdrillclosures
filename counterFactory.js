function counterFactory() {
  try {
    const obj = {
      increment: (varible) => {
        return (varible += 2);
      },
      decrement: (varible) => {
        return (varible -= 1);
      },
    };
    return (function counterVaiable() {
      let counter = 5;
      return `${obj.increment(counter)} ${obj.decrement(counter)}`;
    })();
  } catch (e) {
    console.log(e.message);
  }
}
module.exports = counterFactory;
